# Installation Zakah Project v1.0 [ Website - Massaref ]
    ```git@gitlab.com:grey-shade/zakah-project.git```
1. We will build both of them into seprate theme managed by multisite blugin.
2. Logic and database related will parted as a plugins.
3. Development proccess must goes into parallel branches ```[ master -> website .. massaref-futre -> massaref application ]```
4. All bracnhes must merege at the end of every stage [ devlopment -testing ]

## Minimum System Requirements

October CMS has a few system requirements:

* PHP 5.4 or higher
* PDO PHP Extension
* cURL PHP Extension
* MCrypt PHP Extension
* ZipArchive PHP Library
* GD PHP Library

As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension.
When using Ubuntu, this can be done via ``apt-get install php5-json``.
