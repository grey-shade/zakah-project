<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/subscribe.htm */
class __TwigTemplate_eb0a10dab0bb55eaca096d2754cbf9229c3db80f3d983e6dae5a14daec5d83a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
    <div class=\"row\">
        <form>
            <div class=\"col-sm-8\">
                <input type=\"text\" placeholder=\"Enter your e-mail\" spellcheck=\"false\">
            </div>
            <div class=\"col-sm-4\">
                <button class=\"btn btn-lg btn-primary\" type=\"submit\">
                    Join newsletter
                </button>
            </div>
        </form>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/subscribe.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="container">*/
/*     <div class="row">*/
/*         <form>*/
/*             <div class="col-sm-8">*/
/*                 <input type="text" placeholder="Enter your e-mail" spellcheck="false">*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*                 <button class="btn btn-lg btn-primary" type="submit">*/
/*                     Join newsletter*/
/*                 </button>*/
/*             </div>*/
/*         </form>*/
/*     </div>*/
/* </div>*/
