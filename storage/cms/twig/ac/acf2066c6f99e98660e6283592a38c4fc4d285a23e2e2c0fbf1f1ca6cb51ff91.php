<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/shop/sidebar.htm */
class __TwigTemplate_f2c4f6102ab458d2ac58804cc6b47f0d2e5f16adbaa2a152ce8998ba3a943089 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Search -->
<form role=\"form\">
    <div class=\"well\">
        <div class=\"input-group\">
            <input type=\"text\" class=\"form-control\" placeholder=\"Search products...\">
            <span class=\"input-group-btn\">
                <button class=\"btn btn-primary\" type=\"button\"><i class=\"icon-search\"></i></button>
            </span>
        </div>
    </div>
</form>

<!-- Categories -->
<div class=\"panel-group\" id=\"shopNav\">
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <a data-toggle=\"collapse\" data-parent=\"#shopNav\" href=\"#shopNavOne\">
                Books &amp; CDs
            </a>
        </div>
        <div id=\"shopNavOne\" class=\"panel-collapse collapse in\">
            <ul class=\"list-group filter-list-group\">
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">17</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">8</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">10</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">24</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">4</span></li>
            </ul>
        </div>
    </div>
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <a data-toggle=\"collapse\" data-parent=\"#shopNav\" href=\"#shopNavTwo\" class=\"collapsed\">
                Electronics &amp; Computers
            </a>
        </div>
        <div id=\"shopNavTwo\" class=\"panel-collapse collapse\">
            <ul class=\"list-group filter-list-group\">
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">17</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">8</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">10</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">24</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">4</span></li>
            </ul>
        </div>
    </div>
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <a data-toggle=\"collapse\" data-parent=\"#shopNav\" href=\"#shopNavThree\" class=\"collapsed\">
                Home &amp; Garden
            </a>
        </div>
        <div id=\"shopNavThree\" class=\"panel-collapse collapse\">
            <ul class=\"list-group filter-list-group\">
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">17</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">8</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">10</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">24</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">4</span></li>
            </ul>
        </div>
    </div>
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <a data-toggle=\"collapse\" data-parent=\"#shopNav\" href=\"#shopNavFour\" class=\"collapsed\">
                Toys &amp; Kids
            </a>
        </div>
        <div id=\"shopNavFour\" class=\"panel-collapse collapse\">
            <ul class=\"list-group filter-list-group\">
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">17</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">8</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">10</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">24</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">4</span></li>
            </ul>
        </div>
    </div>
    <div class=\"panel panel-default\">
        <div class=\"panel-heading\">
            <a data-toggle=\"collapse\" data-parent=\"#shopNav\" href=\"#shopNavFive\" class=\"collapsed\">
                Clothing &amp; Shoes
            </a>
        </div>
        <div id=\"shopNavFive\" class=\"panel-collapse collapse\">
            <ul class=\"list-group filter-list-group\">
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">17</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">8</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">10</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">24</span></li>
                <li class=\"list-group-item\"><a href=\"#\">Subcategory</a> <span class=\"badge\">4</span></li>
            </ul>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/shop/sidebar.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <!-- Search -->*/
/* <form role="form">*/
/*     <div class="well">*/
/*         <div class="input-group">*/
/*             <input type="text" class="form-control" placeholder="Search products...">*/
/*             <span class="input-group-btn">*/
/*                 <button class="btn btn-primary" type="button"><i class="icon-search"></i></button>*/
/*             </span>*/
/*         </div>*/
/*     </div>*/
/* </form>*/
/* */
/* <!-- Categories -->*/
/* <div class="panel-group" id="shopNav">*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <a data-toggle="collapse" data-parent="#shopNav" href="#shopNavOne">*/
/*                 Books &amp; CDs*/
/*             </a>*/
/*         </div>*/
/*         <div id="shopNavOne" class="panel-collapse collapse in">*/
/*             <ul class="list-group filter-list-group">*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">17</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">8</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">10</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">24</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">4</span></li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <a data-toggle="collapse" data-parent="#shopNav" href="#shopNavTwo" class="collapsed">*/
/*                 Electronics &amp; Computers*/
/*             </a>*/
/*         </div>*/
/*         <div id="shopNavTwo" class="panel-collapse collapse">*/
/*             <ul class="list-group filter-list-group">*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">17</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">8</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">10</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">24</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">4</span></li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <a data-toggle="collapse" data-parent="#shopNav" href="#shopNavThree" class="collapsed">*/
/*                 Home &amp; Garden*/
/*             </a>*/
/*         </div>*/
/*         <div id="shopNavThree" class="panel-collapse collapse">*/
/*             <ul class="list-group filter-list-group">*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">17</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">8</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">10</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">24</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">4</span></li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <a data-toggle="collapse" data-parent="#shopNav" href="#shopNavFour" class="collapsed">*/
/*                 Toys &amp; Kids*/
/*             </a>*/
/*         </div>*/
/*         <div id="shopNavFour" class="panel-collapse collapse">*/
/*             <ul class="list-group filter-list-group">*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">17</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">8</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">10</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">24</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">4</span></li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/*     <div class="panel panel-default">*/
/*         <div class="panel-heading">*/
/*             <a data-toggle="collapse" data-parent="#shopNav" href="#shopNavFive" class="collapsed">*/
/*                 Clothing &amp; Shoes*/
/*             </a>*/
/*         </div>*/
/*         <div id="shopNavFive" class="panel-collapse collapse">*/
/*             <ul class="list-group filter-list-group">*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">17</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">8</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">10</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">24</span></li>*/
/*                 <li class="list-group-item"><a href="#">Subcategory</a> <span class="badge">4</span></li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
