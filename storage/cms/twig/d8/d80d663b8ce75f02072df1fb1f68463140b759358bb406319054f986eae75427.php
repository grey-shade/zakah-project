<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/footer.htm */
class __TwigTemplate_bf29a001d0b08fde6d8e693e75f0a0fa6a5370e5cfe10fc9de0a1db423f98b9e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
    <nav class=\"pull-left\">
        <ul>
            <li class=\"active\">
                <a href=\"#\">Home</a>
            </li>
            <li>
                <a href=\"#\">Company</a>
            </li>
            <li>
                <a href=\"#\">Portfolio</a>
            </li>
            <li>
                <a href=\"#\">Blog</a>
            </li>
            <li>
                <a href=\"#\">Contact</a>
            </li>
        </ul>
    </nav>
    <div class=\"social-btns pull-right\">
        <a href=\"#\"><div class=\"fui-vimeo\"></div><div class=\"fui-vimeo\"></div></a>
        <a href=\"#\"><div class=\"fui-facebook\"></div><div class=\"fui-facebook\"></div></a>
        <a href=\"#\"><div class=\"fui-twitter\"></div><div class=\"fui-twitter\"></div></a>
    </div>
    <div class=\"additional-links\">
        Be sure to take a look to our <a href=\"#\">Terms of Use</a> and <a href=\"#\">Privacy Policy</a>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <div class="container">*/
/*     <nav class="pull-left">*/
/*         <ul>*/
/*             <li class="active">*/
/*                 <a href="#">Home</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#">Company</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#">Portfolio</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#">Blog</a>*/
/*             </li>*/
/*             <li>*/
/*                 <a href="#">Contact</a>*/
/*             </li>*/
/*         </ul>*/
/*     </nav>*/
/*     <div class="social-btns pull-right">*/
/*         <a href="#"><div class="fui-vimeo"></div><div class="fui-vimeo"></div></a>*/
/*         <a href="#"><div class="fui-facebook"></div><div class="fui-facebook"></div></a>*/
/*         <a href="#"><div class="fui-twitter"></div><div class="fui-twitter"></div></a>*/
/*     </div>*/
/*     <div class="additional-links">*/
/*         Be sure to take a look to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>*/
/*     </div>*/
/* </div>*/
