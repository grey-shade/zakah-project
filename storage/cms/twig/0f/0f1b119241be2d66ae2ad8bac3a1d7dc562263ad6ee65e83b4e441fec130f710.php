<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/pages/home.htm */
class __TwigTemplate_df350e28a011506aaeb570e281f236cea2d1d9901dde2f26ba6c20e00fb7b62e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"home-title\">
    <div class=\"container\">
        <div class=\"row\">
            ";
        // line 5
        echo "            ";
        // line 6
        echo "            ";
        // line 7
        echo "            ";
        // line 8
        echo "            ";
        // line 9
        echo "            ";
        // line 10
        echo "            ";
        // line 11
        echo "            ";
        // line 12
        echo "            ";
        // line 13
        echo "            ";
        // line 14
        echo "            ";
        // line 15
        echo "            ";
        // line 16
        echo "            ";
        // line 17
        echo "            ";
        // line 18
        echo "            ";
        // line 19
        echo "            ";
        // line 20
        echo "            ";
        // line 21
        echo "            ";
        // line 22
        echo "            ";
        // line 23
        echo "            ";
        // line 24
        echo "            ";
        // line 25
        echo "            ";
        // line 26
        echo "            ";
        // line 27
        echo "            ";
        // line 28
        echo "            ";
        // line 29
        echo "            ";
        // line 30
        echo "            ";
        // line 31
        echo "            ";
        // line 32
        echo "            ";
        // line 33
        echo "            ";
        // line 34
        echo "        </div>
    </div>
</section>

";
        // line 47
        echo "
<br />

<div class=\"container\">

    <!-- Services -->
    <div class=\"row our-services\">
        <div class=\"col-sm-12\">
            <h4 class=\"headline\"><span>What we do</span></h4>
            <p>He hasn't got a freckle my mad as a middy. Trent from punchy maccas no dramas shazza got us some ripper. As dry as a bradman flamin lets throw a cut lunch commando.</p>

            <div class=\"row\">
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-gear\"></i>
                            <div class=\"service-desc\">
                                <h5>Built with Bootstrap 3</h5>
                                <p>Bacon ipsum dolor sit amet rump tri-tip pig pork chop. Doner tri-tip shoulder, andouille sausage filet mignon beef ribs drumstick.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-laptop\"></i>
                            <div class=\"service-desc\">
                                <h5>Responsive design</h5>
                                <p>Salami turkey jowl rump jerky ball tip drumstick ground round tenderloin kielbasa hamburger prosciutto. </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-refresh\"></i>
                            <div class=\"service-desc\">
                                <h5>Easy to customize</h5>
                                <p>T-bone tongue turducken hamburger, doner sirloin rump pork jowl cow. Beef rump ham hock brisket.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-plus\"></i>
                            <div class=\"service-desc\">
                                <h5>Works out of the box</h5>
                                <p>Pork belly chicken turkey andouille t-bone ribeye tenderloin prosciutto flank chuck porchetta.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-envelope\"></i>
                            <div class=\"service-desc\">
                                <h5>24/7 support</h5>
                                <p>Kielbasa chicken doner filet mignon meatball salami ground round prosciutto spare ribs jerky meatloaf shankle bacon beef swine.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"col-sm-4\">
                    <div class=\"services\">
                        <div class=\"service-item\">
                            <i class=\"icon-picture\"></i>
                            <div class=\"service-desc\">
                                <h5>Special Portfolio</h5>
                                <p>Short loin pork belly beef, tenderloin swine ribeye tri-tip doner salami rump hamburger kielbasa venison beef ribs brisket.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class=\"recent-tweets\">
        <h4 class=\"headline\"><span>Recent tweets</span></h4>
        <div class=\"tweet\">
            <i class=\"icon-twitter\"></i>
            <p>
                Salami turkey jowl rump jerky ball tip drumstick ground round tenderloin kielbasa hamburger prosciutto.
                <a href=\"#\">1 day ago</a>
            </p>
        </div>
        <div class=\"tweet\">
            <i class=\"icon-twitter\"></i>
            <p>
                Short loin pork belly beef, tenderloin swine ribeye tri-tip doner salami rump hamburger.
                <a href=\"#\">2 days ago</a>
            </p>
        </div>
    </div>

</div>

<br />";
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/pages/home.htm";
    }

    public function getDebugInfo()
    {
        return array (  88 => 47,  82 => 34,  80 => 33,  78 => 32,  76 => 31,  74 => 30,  72 => 29,  70 => 28,  68 => 27,  66 => 26,  64 => 25,  62 => 24,  60 => 23,  58 => 22,  56 => 21,  54 => 20,  52 => 19,  50 => 18,  48 => 17,  46 => 16,  44 => 15,  42 => 14,  40 => 13,  38 => 12,  36 => 11,  34 => 10,  32 => 9,  30 => 8,  28 => 7,  26 => 6,  24 => 5,  19 => 1,);
    }
}
/* <section class="home-title">*/
/*     <div class="container">*/
/*         <div class="row">*/
/*             {#<div class="col-sm-4">#}*/
/*             {#    <h3>Content</h3>#}*/
/*             {#    <p>You have the design, you have the code. We've created the product that will help your startup to look even better.</p>#}*/
/*             {#    <div class="signup-form">#}*/
/*             {#        <form>#}*/
/*             {#            <div class="form-group">#}*/
/*             {#                <input class="form-control" type="text" placeholder="Your E-mail">#}*/
/*             {#            </div>#}*/
/*             {#            <div class="form-group">#}*/
/*             {#                <div>#}*/
/*             {#                    <input type="password" class="form-control" placeholder="Password">#}*/
/*             {#                </div>#}*/
/*             {#                <div>#}*/
/*             {#                    <input type="password" class="form-control" placeholder="Confirmation">#}*/
/*             {#                </div>#}*/
/*             {#            </div>#}*/
/*             {#            <div class="form-group">#}*/
/*             {#                <button type="submit" class="btn btn-block btn-info">Sign Up</button>#}*/
/*             {#            </div>#}*/
/*             {#        </form>#}*/
/*             {#    </div>#}*/
/*             {#    <div class="additional-links">#}*/
/*             {#        By signing up you agree to <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>#}*/
/*             {#    </div>#}*/
/*             {#</div>#}*/
/*             {#<div class="col-sm-7 col-sm-offset-1 player-wrapper">#}*/
/*             {#    <div class="player">#}*/
/*             {#        <iframe src="http://player.vimeo.com/video/29568236?color=3498db" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>#}*/
/*             {#    </div>#}*/
/*             {#</div>#}*/
/*         </div>*/
/*     </div>*/
/* </section>*/
/* */
/* {#<section class="logos">#}*/
/* {#    <div class="container">#}*/
/* {#        <div><img src="{{ 'assets/images/logos/mashable.png'|theme }}" alt=""></div>#}*/
/* {#        <div><img src="{{ 'assets/images/logos/guardian.png'|theme }}" alt=""></div>#}*/
/* {#        <div><img src="{{ 'assets/images/logos/forbes.png'|theme }}" alt=""></div>#}*/
/* {#        <div><img src="{{ 'assets/images/logos/red-bull.png'|theme }}" alt=""></div>#}*/
/* {#        <div><img src="{{ 'assets/images/logos/ny-times.png'|theme }}" alt=""></div>#}*/
/* {#    </div>#}*/
/* {#</section>#}*/
/* */
/* <br />*/
/* */
/* <div class="container">*/
/* */
/*     <!-- Services -->*/
/*     <div class="row our-services">*/
/*         <div class="col-sm-12">*/
/*             <h4 class="headline"><span>What we do</span></h4>*/
/*             <p>He hasn't got a freckle my mad as a middy. Trent from punchy maccas no dramas shazza got us some ripper. As dry as a bradman flamin lets throw a cut lunch commando.</p>*/
/* */
/*             <div class="row">*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-gear"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>Built with Bootstrap 3</h5>*/
/*                                 <p>Bacon ipsum dolor sit amet rump tri-tip pig pork chop. Doner tri-tip shoulder, andouille sausage filet mignon beef ribs drumstick.</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-laptop"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>Responsive design</h5>*/
/*                                 <p>Salami turkey jowl rump jerky ball tip drumstick ground round tenderloin kielbasa hamburger prosciutto. </p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-refresh"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>Easy to customize</h5>*/
/*                                 <p>T-bone tongue turducken hamburger, doner sirloin rump pork jowl cow. Beef rump ham hock brisket.</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="row">*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-plus"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>Works out of the box</h5>*/
/*                                 <p>Pork belly chicken turkey andouille t-bone ribeye tenderloin prosciutto flank chuck porchetta.</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-envelope"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>24/7 support</h5>*/
/*                                 <p>Kielbasa chicken doner filet mignon meatball salami ground round prosciutto spare ribs jerky meatloaf shankle bacon beef swine.</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*                 <div class="col-sm-4">*/
/*                     <div class="services">*/
/*                         <div class="service-item">*/
/*                             <i class="icon-picture"></i>*/
/*                             <div class="service-desc">*/
/*                                 <h5>Special Portfolio</h5>*/
/*                                 <p>Short loin pork belly beef, tenderloin swine ribeye tri-tip doner salami rump hamburger kielbasa venison beef ribs brisket.</p>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*         </div>*/
/*     </div>*/
/* */
/*     <div class="recent-tweets">*/
/*         <h4 class="headline"><span>Recent tweets</span></h4>*/
/*         <div class="tweet">*/
/*             <i class="icon-twitter"></i>*/
/*             <p>*/
/*                 Salami turkey jowl rump jerky ball tip drumstick ground round tenderloin kielbasa hamburger prosciutto.*/
/*                 <a href="#">1 day ago</a>*/
/*             </p>*/
/*         </div>*/
/*         <div class="tweet">*/
/*             <i class="icon-twitter"></i>*/
/*             <p>*/
/*                 Short loin pork belly beef, tenderloin swine ribeye tri-tip doner salami rump hamburger.*/
/*                 <a href="#">2 days ago</a>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* */
/* </div>*/
/* */
/* <br />*/
