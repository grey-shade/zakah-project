<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/nav.htm */
class __TwigTemplate_6a9462b6d35291a1b902e617c575f2cb7033ea62181a544daa0c70398b272845 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 41
        echo "
";
        // line 44
        echo "
";
        // line 65
        echo "
";
        // line 66
        $context["nav"] = $this;
        // line 67
        echo "
<nav id=\"layout-nav\" class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\" >
    <div class=\"container\">
        <div class=\"navbar-header\">
            ";
        // line 72
        echo "            ";
        // line 73
        echo "            ";
        // line 74
        echo "            ";
        // line 75
        echo "            ";
        // line 76
        echo "            ";
        // line 77
        echo "            ";
        // line 78
        echo "        </div>
        <div class=\"collapse navbar-collapse navbar-main-collapse\">
            <ul class=\"nav navbar-nav navbar-right\">
                ";
        // line 81
        echo $context["nav"]->getrender_menu((isset($context["links"]) ? $context["links"] : null));
        echo "
                <li>
                    
                    ";
        // line 85
        echo "                    ";
        // line 86
        echo "                    ";
        // line 87
        echo "                    ";
        // line 88
        echo "                    ";
        // line 89
        echo "                </li>
            </ul>
        </div>
    </div>
</nav>";
    }

    // line 45
    public function getrender_menu($__links__ = null)
    {
        $context = $this->env->mergeGlobals(array(
            "links" => $__links__,
            "varargs" => func_num_args() > 1 ? array_slice(func_get_args(), 1) : array(),
        ));

        $blocks = array();

        ob_start();
        try {
            // line 46
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
            foreach ($context['_seq'] as $context["code"] => $context["link"]) {
                // line 47
                echo "        <li class=\"";
                echo ((($context["code"] == (isset($context["currentPage"]) ? $context["currentPage"] : null))) ? ("active") : (""));
                echo " ";
                echo (($this->getAttribute($context["link"], "sublinks", array())) ? ("dropdown") : (""));
                echo "\">
            <a
                href=\"";
                // line 49
                echo (($this->getAttribute($context["link"], "sublinks", array())) ? ("#") : ($this->env->getExtension('CMS')->pageFilter((($this->getAttribute($context["link"], "page", array())) ? ($this->getAttribute($context["link"], "page", array())) : ($this->getAttribute($context["link"], 0, array(), "array"))))));
                echo "\"
                ";
                // line 50
                if ($this->getAttribute($context["link"], "sublinks", array())) {
                    echo "data-toggle=\"dropdown\"";
                }
                // line 51
                echo "                class=\"";
                echo (($this->getAttribute($context["link"], "sublinks", array())) ? ("dropdown-toggle") : (""));
                echo "\"
            >
                ";
                // line 53
                echo twig_escape_filter($this->env, (($this->getAttribute($context["link"], "name", array())) ? ($this->getAttribute($context["link"], "name", array())) : ($this->getAttribute($context["link"], 1, array(), "array"))), "html", null, true);
                echo "
                ";
                // line 54
                if ($this->getAttribute($context["link"], "sublinks", array())) {
                    echo "<span class=\"caret\"></span>";
                }
                // line 55
                echo "            </a>
            ";
                // line 56
                if ($this->getAttribute($context["link"], "sublinks", array())) {
                    // line 57
                    echo "                <span class=\"dropdown-arrow\"></span>
                <ul class=\"dropdown-menu\">
                    ";
                    // line 59
                    echo $this->getAttribute($this, "render_menu", array(0 => $this->getAttribute($context["link"], "sublinks", array())), "method");
                    echo "
                </ul>
            ";
                }
                // line 62
                echo "        </li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['code'], $context['link'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/partials/nav.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 62,  131 => 59,  127 => 57,  125 => 56,  122 => 55,  118 => 54,  114 => 53,  108 => 51,  104 => 50,  100 => 49,  92 => 47,  87 => 46,  75 => 45,  67 => 89,  65 => 88,  63 => 87,  61 => 86,  59 => 85,  53 => 81,  48 => 78,  46 => 77,  44 => 76,  42 => 75,  40 => 74,  38 => 73,  36 => 72,  30 => 67,  28 => 66,  25 => 65,  22 => 44,  19 => 41,);
    }
}
/* {# Note: Only one levels of sublinks are supported by Bootstrap 3 #}*/
/* {#{% set#}*/
/* {#     links = {#}*/
/* {#        'home': ['home', 'Home'],#}*/
/* {#        'pages': {#}*/
/* {#            name: 'Pages',#}*/
/* {#            sublinks: {#}*/
/* {#                'about':         ['samples/about', 'About Us'],#}*/
/* {#                'contact':       ['samples/contact', 'Contact Us'],#}*/
/* {#                'pricing-table': ['samples/pricing-table', 'Pricing Table'],#}*/
/* {#                'services':      ['samples/services', 'Services'],#}*/
/* {#                'signin':        ['samples/signin', 'Sign In'],#}*/
/* {#                'register':      ['samples/register', 'Register'],#}*/
/* {#                'error':         ['error', 'Error Page'],#}*/
/* {#                '404':           ['404', '404 Page'],#}*/
/* {#            },#}*/
/* {#        },#}*/
/* {#        'portfolio': {#}*/
/* {#            name: 'Portfolio',#}*/
/* {#            sublinks: {#}*/
/* {#                'portfolio': ['portfolio/portfolio', 'Portfolio'],#}*/
/* {#                'project': ['portfolio/project', 'Project'],#}*/
/* {#            },#}*/
/* {#        },#}*/
/* {#        'blog': {#}*/
/* {#            name: 'Blog',#}*/
/* {#            sublinks: {#}*/
/* {#                'blog': ['blog/blog', 'Blog'],#}*/
/* {#                'post': ['blog/post', 'Blog Post'],#}*/
/* {#            },#}*/
/* {#        },#}*/
/* {#        'shop': {#}*/
/* {#            name: 'Shop',#}*/
/* {#            sublinks: {#}*/
/* {#                'shop': ['shop/shop', 'Shop'],#}*/
/* {#                'product': ['shop/product', 'Product'],#}*/
/* {#                'cart': ['shop/cart', 'Cart'],#}*/
/* {#            },#}*/
/* {#        },#}*/
/* {#        'ui-elements': ['ui-elements', 'UI Elements'],#}*/
/* */
/* {#    }#}*/
/* {#%}#}*/
/* */
/* {% macro render_menu(links) %}*/
/*     {% for code, link in links %}*/
/*         <li class="{{ code == currentPage ? 'active' }} {{ link.sublinks ? 'dropdown' }}">*/
/*             <a*/
/*                 href="{{ link.sublinks ? '#' : (link.page ?: link[0])|page }}"*/
/*                 {% if link.sublinks %}data-toggle="dropdown"{% endif %}*/
/*                 class="{{ link.sublinks ? 'dropdown-toggle' }}"*/
/*             >*/
/*                 {{ link.name ?: link[1] }}*/
/*                 {% if link.sublinks %}<span class="caret"></span>{% endif %}*/
/*             </a>*/
/*             {% if link.sublinks %}*/
/*                 <span class="dropdown-arrow"></span>*/
/*                 <ul class="dropdown-menu">*/
/*                     {{ _self.render_menu(link.sublinks) }}*/
/*                 </ul>*/
/*             {% endif %}*/
/*         </li>*/
/*     {% endfor %}*/
/* {% endmacro %}*/
/* */
/* {% import _self as nav %}*/
/* */
/* <nav id="layout-nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation" >*/
/*     <div class="container">*/
/*         <div class="navbar-header">*/
/*             {#<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">#}*/
/*             {#    <span class="sr-only">Toggle navigation</span>#}*/
/*             {#    <span class="icon-bar"></span>#}*/
/*             {#    <span class="icon-bar"></span>#}*/
/*             {#    <span class="icon-bar"></span>#}*/
/*             {#</button>#}*/
/*             {#<a class="navbar-brand" href="{{ 'home'|page }}">Flat</a>#}*/
/*         </div>*/
/*         <div class="collapse navbar-collapse navbar-main-collapse">*/
/*             <ul class="nav navbar-nav navbar-right">*/
/*                 {{ nav.render_menu(links) }}*/
/*                 <li>*/
/*                     */
/*                     {#<button#}*/
/*                     {#    onclick="window.location='{{ 'samples/signin'|page }}'"#}*/
/*                     {#    class="btn btn-sm navbar-btn btn-primary navbar-right hidden-sm hidden-xs">#}*/
/*                     {#    Sign in#}*/
/*                     {#</button>#}*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/*     </div>*/
/* </nav>*/
