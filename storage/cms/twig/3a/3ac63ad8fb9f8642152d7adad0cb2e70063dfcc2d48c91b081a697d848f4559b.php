<?php

/* /opt/lampp/htdocs/zakah-project/themes/responsiv-flat/pages/shop/shop.htm */
class __TwigTemplate_2a1ae6612c3977d2ca93bb8f635458060dbe782b56b94bd7750c8a71b3ecbb89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section id=\"layout-title\">
    <div class=\"container\">
        <h3>Shop</h3>
    </div>
</section>

<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-sm-4\">
            ";
        // line 10
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('CMS')->partialFunction("shop/sidebar"        , $context['__cms_partial_params']        );
        unset($context['__cms_partial_params']);
        // line 11
        echo "        </div>
        <div class=\"col-sm-8\">
            <!-- Filters -->
            <ul class=\"nav nav-tabs nav-justified\">
                <li class=\"active\"><a href=\"#popular\" data-toggle=\"tab\">Popular</a></li>
                <li><a href=\"#recommended\" data-toggle=\"tab\">Recommended</a></li>
                <li><a href=\"#recent\" data-toggle=\"tab\">New Products</a></li>
                <li class=\"hidden-sm\"><a href=\"#last\" data-toggle=\"tab\">Last Chance</a></li>
            </ul>

            <div class=\"row\">
                <div class=\"tab-content\">

                    <!-- All-->
                    <div class=\"tab-pane fade in active\" id=\"popular\">
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product featured\">
                                <a href=\"";
        // line 28
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/imac.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 29
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #1</h6></a>
                                <p class=\"price\">
                                    <span class=\"old\">\$80.99</span>
                                    <span class=\"new\">\$59.99</span>
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 39
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/macbook.jpg");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 40
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #2</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 49
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/ipad.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 50
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #3</h6></a>
                                <p>
                                    \$50.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 59
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/imac.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 60
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #1</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 69
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/macbook.jpg");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 70
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #2</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 79
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/ipad.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 80
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #3</h6></a>
                                <p>
                                    \$50.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                    </div>

                    <!-- Ebooks -->
                    <div class=\"tab-pane fade\" id=\"recommended\">
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product featured\">
                                <a href=\"";
        // line 93
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/imac.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 94
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #1</h6></a>
                                <p class=\"price\">
                                    <span class=\"old\">\$80.99</span>
                                    <span class=\"new\">\$59.99</span>
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 104
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/macbook.jpg");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 105
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #2</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                    </div>

                    <!-- Audio CD -->
                    <div class=\"tab-pane fade\" id=\"recent\">
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 118
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/ipad.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 119
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #3</h6></a>
                                <p>
                                    \$50.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 128
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/imac.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 129
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #1</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 138
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/macbook.jpg");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 139
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #2</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                    </div>

                    <!-- Audio CD -->
                    <div class=\"tab-pane fade\" id=\"last\">
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 152
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/ipad.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 153
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #3</h6></a>
                                <p>
                                    \$50.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 162
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/ipad.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 163
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #3</h6></a>
                                <p>
                                    \$50.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 172
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/imac.png");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 173
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #1</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                        <div class=\"col-sm-6 col-lg-4\">
                            <div class=\"shop-product\">
                                <a href=\"";
        // line 182
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><img src=\"";
        echo $this->env->getExtension('CMS')->themeFilter("assets/images/shop/macbook.jpg");
        echo "\" class=\"img-responsive\" alt=\"\"></a>
                                <a href=\"";
        // line 183
        echo $this->env->getExtension('CMS')->pageFilter("shop/product");
        echo "\"><h6>Product #2</h6></a>
                                <p>
                                    \$200.00
                                </p>
                                <a href=\"#\" class=\"btn btn-sm btn-info\"><i class=\"icon-shopping-cart\"></i> Add to cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pagination -->
            <div class=\"pagination pull-right\">
                <ul>
                    <li class=\"previous\"><a href=\"#fakelink\" class=\"fui-arrow-left\"></a></li>
                    <li class=\"active\"><a href=\"#fakelink\">1</a></li>
                    <li><a href=\"#fakelink\">2</a></li>
                    <li><a href=\"#fakelink\">3</a></li>
                    <li><a href=\"#fakelink\">4</a></li>
                    <li><a href=\"#fakelink\">5</a></li>
                    <li><a href=\"#fakelink\">6</a></li>
                    <li><a href=\"#fakelink\">7</a></li>
                    <li><a href=\"#fakelink\">8</a></li>
                    <li class=\"next\"><a href=\"#fakelink\" class=\"fui-arrow-right\"></a></li>
                </ul>
            </div>

        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/opt/lampp/htdocs/zakah-project/themes/responsiv-flat/pages/shop/shop.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  325 => 183,  319 => 182,  307 => 173,  301 => 172,  289 => 163,  283 => 162,  271 => 153,  265 => 152,  249 => 139,  243 => 138,  231 => 129,  225 => 128,  213 => 119,  207 => 118,  191 => 105,  185 => 104,  172 => 94,  166 => 93,  150 => 80,  144 => 79,  132 => 70,  126 => 69,  114 => 60,  108 => 59,  96 => 50,  90 => 49,  78 => 40,  72 => 39,  59 => 29,  53 => 28,  34 => 11,  30 => 10,  19 => 1,);
    }
}
/* <section id="layout-title">*/
/*     <div class="container">*/
/*         <h3>Shop</h3>*/
/*     </div>*/
/* </section>*/
/* */
/* <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-sm-4">*/
/*             {% partial 'shop/sidebar' %}*/
/*         </div>*/
/*         <div class="col-sm-8">*/
/*             <!-- Filters -->*/
/*             <ul class="nav nav-tabs nav-justified">*/
/*                 <li class="active"><a href="#popular" data-toggle="tab">Popular</a></li>*/
/*                 <li><a href="#recommended" data-toggle="tab">Recommended</a></li>*/
/*                 <li><a href="#recent" data-toggle="tab">New Products</a></li>*/
/*                 <li class="hidden-sm"><a href="#last" data-toggle="tab">Last Chance</a></li>*/
/*             </ul>*/
/* */
/*             <div class="row">*/
/*                 <div class="tab-content">*/
/* */
/*                     <!-- All-->*/
/*                     <div class="tab-pane fade in active" id="popular">*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product featured">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/imac.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #1</h6></a>*/
/*                                 <p class="price">*/
/*                                     <span class="old">$80.99</span>*/
/*                                     <span class="new">$59.99</span>*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/macbook.jpg'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #2</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/ipad.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #3</h6></a>*/
/*                                 <p>*/
/*                                     $50.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/imac.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #1</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/macbook.jpg'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #2</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/ipad.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #3</h6></a>*/
/*                                 <p>*/
/*                                     $50.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <!-- Ebooks -->*/
/*                     <div class="tab-pane fade" id="recommended">*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product featured">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/imac.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #1</h6></a>*/
/*                                 <p class="price">*/
/*                                     <span class="old">$80.99</span>*/
/*                                     <span class="new">$59.99</span>*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/macbook.jpg'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #2</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <!-- Audio CD -->*/
/*                     <div class="tab-pane fade" id="recent">*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/ipad.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #3</h6></a>*/
/*                                 <p>*/
/*                                     $50.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/imac.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #1</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/macbook.jpg'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #2</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/* */
/*                     <!-- Audio CD -->*/
/*                     <div class="tab-pane fade" id="last">*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/ipad.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #3</h6></a>*/
/*                                 <p>*/
/*                                     $50.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/ipad.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #3</h6></a>*/
/*                                 <p>*/
/*                                     $50.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/imac.png'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #1</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                         <div class="col-sm-6 col-lg-4">*/
/*                             <div class="shop-product">*/
/*                                 <a href="{{ 'shop/product'|page }}"><img src="{{ 'assets/images/shop/macbook.jpg'|theme }}" class="img-responsive" alt=""></a>*/
/*                                 <a href="{{ 'shop/product'|page }}"><h6>Product #2</h6></a>*/
/*                                 <p>*/
/*                                     $200.00*/
/*                                 </p>*/
/*                                 <a href="#" class="btn btn-sm btn-info"><i class="icon-shopping-cart"></i> Add to cart</a>*/
/*                             </div>*/
/*                         </div>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/* */
/*             <!-- Pagination -->*/
/*             <div class="pagination pull-right">*/
/*                 <ul>*/
/*                     <li class="previous"><a href="#fakelink" class="fui-arrow-left"></a></li>*/
/*                     <li class="active"><a href="#fakelink">1</a></li>*/
/*                     <li><a href="#fakelink">2</a></li>*/
/*                     <li><a href="#fakelink">3</a></li>*/
/*                     <li><a href="#fakelink">4</a></li>*/
/*                     <li><a href="#fakelink">5</a></li>*/
/*                     <li><a href="#fakelink">6</a></li>*/
/*                     <li><a href="#fakelink">7</a></li>*/
/*                     <li><a href="#fakelink">8</a></li>*/
/*                     <li class="next"><a href="#fakelink" class="fui-arrow-right"></a></li>*/
/*                 </ul>*/
/*             </div>*/
/* */
/*         </div>*/
/*     </div>*/
/* </div>*/
